import * as http from 'http';
import Api from './Api';
import sequelize from './Db';
const config = require('./config/env/config')();
const server = http.createServer(Api);

function normalizePort(val) {
    var port = parseInt(val, 10);
  
    if (isNaN(port)) {
      // named pipe
      return val;
    }
  
    if (port >= 0) {
      // port number
      return port;
    }
  
    return false;
  }

/**
 * Boot the server after configuring and connecting the Database
 */
sequelize.sync({force:false})
    .then(() => {
        
        console.log("Sincronização do banco de dados realizada.");
        var port = normalizePort(process.env.PORT || config.serverPort);
        server.listen(port);
        server.on('listening', () => console.log(`Servidor HTTP iniciado na porta ${port}`))
        server.on('error', (error: NodeJS.ErrnoException) => console.log(`Ocorreu um erro: ${error}`));

    })
    .catch(error=> {
        console.log(error);
    })


  