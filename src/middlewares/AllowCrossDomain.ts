import {Router, Request, Response, NextFunction} from 'express';

/**
 * Class that configures HTTP headers
 */
export class AllowCrossDomain {

    constructor() {}

    /**
     * Middleware method that allows requests GET,PUT,POST,DELETE,OPTIONS
     * @param req 
     * @param res 
     * @param next 
     */
    allowCrossDomain(req: Request, res: Response, next: NextFunction) {

        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

        if (req.method == 'OPTIONS') {
            res.sendStatus(200);
        }
        else {
            next();
        }
    }
}