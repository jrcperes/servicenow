import { Model} from 'sequelize-typescript';
import * as HttpStatus from 'http-status'
let axios = require('axios')

import { IncidentEntity} from './../entity/IncidentEntity';
import { Subcategory } from "./../models/Subcategory";
import { SysUser } from "./../models/SysUser";
import { ProductType } from "./../models/ProductType";
import { CoreCompany } from "./../models/CoreCompany";
import { Category } from "./../models/Category";
import { Units } from "./../models/Units";
import { SysUserGroup } from "./../models/SysUserGroup";
import { CMNLocation } from "./../models/CMNLocation";
import { Incident } from "./../models/Incident";
const config  = require('./../config/env/config')();

/**
 * Class of import service
 */
export class ImportService {

    constructor() {}

    /**
    * Service to import
    * @returns Returns 
    */
    async import(table: string) {

        let URL = 'https://edp.service-now.com/api/now/table/incident?sysparm_query=assignment_group=javascript:getMyGroups()^opened_at%3E=javascript:gs.beginningOfMonth()^ORu_resolve_time%3E=javascript:gs.beginningOfMonth()^ORu_resolve_timeISEMPTY';

        let data = await this.loadFromURL(URL);
        let itens = data["result"];

        for (const item of itens) {
            await this.createIfNotExists(item.u_operador, SysUser);
            await this.createIfNotExists(item.u_subcategory, Subcategory);
            await this.createIfNotExists(item.u_caller, SysUser);
            await this.createIfNotExists(item.assigned_to, SysUser);
            await this.createIfNotExists(item.u_product_type, ProductType);
            await this.createIfNotExists(item.u_companyincidencia, CoreCompany);
            await this.createIfNotExists(item.u_category, Category);
            await this.createIfNotExists(item.u_unitincidencia, Units);
            await this.createIfNotExists(item.opened_by, SysUser);
            await this.createIfNotExists(item.sys_domain, SysUserGroup);
            await this.createIfNotExists(item.caller_id, SysUser);
            await this.createIfNotExists(item.u_gestor_de_incidencias, SysUser);
            await this.createIfNotExists(item.assignment_group, SysUserGroup);
            await this.createIfNotExists(item.company, CoreCompany);
            await this.createIfNotExists(item.u_locationincidencia, CMNLocation);
            await this.createIfNotExists(item.location, CMNLocation);

            let incidentEntity = new IncidentEntity(item);
            console.log(incidentEntity);
            await this.createIfNotExists(incidentEntity, Incident);
        }

        // let appCredentials : AppCredentials = await AppCredentials.scope('full').findOne<AppCredentials>({
        //     where: {login: login}
        // });

        // if (appCredentials) {
        //     if (appCredentials.password == password) {
        //         const token = jwt.sign(
        //             { userId: login, username: login },
        //             config.secretJWT,
        //             { expiresIn: "1h" }
        //         );
        //         return token;
        //     }
        // } 
      
    }

    private generateWhere(sourceName: string, object: any) {
        let where = null;
        if (sourceName != 'Incident') {
            where = {
                value: object.value
            };
        } else {
            where = {
                number: object.number
            };
        }
        return where;
    }

    async createIfNotExists(object: any, source: any): Promise<object> {
        if (object != null && object != '') {
            let aRecord = await source.scope('full').findOne({
                where: this.generateWhere(source.name, object)
            });
            if (aRecord == null) {
                return await source.create(object);
            }
            return aRecord;
        }
    }

    async loadFromURL(url: string): Promise<object> {

        try {
            let options = {
                method: 'get',
                url: url,
                headers: { "Authorization": "Basic U0EtQlItSU5EUkFfU2VydmljZW5vdzoxQE1udEAjJQ==" }
            }
            const response = await axios(options);
            if (response.status == HttpStatus.OK) {
                return response.data;
            } else {
                return null;
            }
        } catch (err) {
            return err;
        }
    }
}