/**
 * Class that defines the generic standard for persistence
 */
export class GenericEntity {

    value: string;
    link: string;
}