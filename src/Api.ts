import * as express from 'express';
import { Application } from 'express';
import * as morgan from 'morgan';
import * as bodyParser from 'body-parser';

import Routes from './routes/Routes';
import {AllowCrossDomain} from './middlewares/AllowCrossDomain';

const helmet = require('helmet');

/**
 * Main API class for defining middleware and routes
 */
class Api {
    
    /**
     * Express object
     */
    public express: Application;
    
    constructor() {
        this.express = express();
        this.middleware();
        this.router(this.express);
    }

    /**
     * Method that defines middleware
     */
    middleware(): void {
        this.express.use(helmet());
        this.express.use(morgan('dev'));
        this.express.use(bodyParser.urlencoded({extended: true}));
        this.express.use(bodyParser.json());
        this.express.use(new AllowCrossDomain().allowCrossDomain);
    }

    /**
     * Method for starting route definitions
     * @param app Express Application object
     */
    private router(app: Application): void {
        Routes.initRoutes(app);
    }
}

export default new Api().express;
