var path = require('path');

/**
 * Configuration by environment
 */

let extension: string = path.extname(__filename);
console.log("Environment: " + process.env.NODE_ENV);
module.exports = () => require(`../env/${ process.env.NODE_ENV }.env${extension}`);