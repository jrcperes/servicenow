import { Request, Response } from 'express';
import * as HttpStatus from 'http-status';
import { ImportService } from '../services/ImportService';

/**
 * Class that represents import controller
 */
export class ImportController {

    private importService;

    /**
     * Constructor of the class that instantiates the import service
     */
    constructor() {
        this.importService = new ImportService(); 
    }

    /**
     * Method for import
     */
    public import = async (req: Request, res: Response) => {
        
        try{
            let table = req.params.table;

            let resp = await this.importService.import(table);
            return res.status(HttpStatus.OK).json({resp: resp});

        } catch (erro) {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR).send(`Erro ao efetuar a importação: ${erro.message}`);
        }            
    }
    
}

