import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "sys_user"
})

/**
 * Class that represents the sys_user model
 */
export class SysUser extends Model<SysUser> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'u_operador')
    u_operador_incident: Incident[];

    @HasMany(() => Incident, 'u_caller')
    u_caller_incident: Incident[];

    @HasMany(() => Incident, 'assigned_to')
    assigned_to_incident: Incident[];

    @HasMany(() => Incident, 'opened_by')
    opened_by_incident: Incident[];

    @HasMany(() => Incident, 'closed_by')
    closed_by_incident: Incident[];

    @HasMany(() => Incident, 'caller_id')
    caller_id_incident: Incident[];

    @HasMany(() => Incident, 'u_gestor_de_incidencias')
    u_gestor_de_incidencias_incident: Incident[];
}