import { Table, Model, PrimaryKey, ForeignKey, DataType, Column, AutoIncrement, Scopes, BelongsTo} from 'sequelize-typescript';

import { Subcategory } from "./Subcategory";
import { SysUser } from "./SysUser";
import { ProductType } from "./ProductType";
import { CoreCompany } from "./CoreCompany";
import { Category } from "./Category";
import { Units } from "./Units";
import { SysUserGroup } from "./SysUserGroup";
import { CMNLocation } from "./CMNLocation";

@Scopes({
    full:{
        attributes: ['number', 'promoted_by']
    }
})
@Table({
    timestamps: false,
    tableName: "incident"
})

/**
 * Class that represents the incident model
 */
export class Incident extends Model<Incident> {

    @PrimaryKey
    @Column(DataType.TEXT)
    number: string;

    @Column(DataType.TEXT)
    promoted_by: string;

    @Column(DataType.TEXT)
    parent: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    u_operador: string;
    
    @Column(DataType.TEXT)
    caused_by: string;

    @Column(DataType.TEXT)
    u_resolution: string;
    
    @Column(DataType.TEXT)
    watch_list: string;

    @Column(DataType.TEXT)
    upon_reject: string;

    @Column(DataType.TEXT)
    sys_updated_on: string;

    @Column(DataType.TEXT)
    approval_history: string;

    @Column(DataType.TEXT)
    skills: string;

    @Column(DataType.TEXT)
    proposed_by: string;

    @Column(DataType.TEXT)
    u_w_notes: string;

    @Column(DataType.TEXT)
    u_unit: string;

    @Column(DataType.TEXT)
    lessons_learned: string;

    @Column(DataType.TEXT)
    u_edp_security: string;

    @Column(DataType.TEXT)
    state: string;

    @Column(DataType.TEXT)
    sys_created_by: string;

    @Column(DataType.TEXT)
    knowledge: string;

    @Column(DataType.TEXT)
    order: string;

    @Column(DataType.TEXT)
    u_replan_count: string;

    @Column(DataType.TEXT)
    u_tempdate: string;

    @ForeignKey(() => Subcategory)
    @Column({
        type: DataType.TEXT
    })
    u_subcategory: string;

    @Column(DataType.TEXT)
    cmdb_ci: string;

    @Column(DataType.TEXT)
    delivery_plan: string;
    
    @Column(DataType.TEXT)
    impact: string;

    @Column(DataType.TEXT)
    u_subestado: string;

    @Column(DataType.TEXT)
    active: string;

    @Column(DataType.TEXT)
    u_relates_incidents: string;

    @Column(DataType.TEXT)
    priority: string;

    @Column(DataType.TEXT)
    u_resolve_time: string;

    @Column(DataType.TEXT)
    business_duration: string;

    @Column(DataType.TEXT)
    group_list: string;

    @Column(DataType.TEXT)
    u_show_in_portal: string;

    @Column(DataType.TEXT)
    approval_set: string;

    @Column(DataType.TEXT)
    u_retipif_count: string;

    @Column(DataType.TEXT)
    major_incident_state: string;

    @Column(DataType.TEXT)
    short_description: string;

    @Column(DataType.TEXT)
    correlation_display: string;

    @Column(DataType.TEXT)
    work_start: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    u_caller: string;
    
    @Column(DataType.TEXT)
    u_check_replace: string;

    @Column(DataType.TEXT)
    u_falso_positivo: string;

    @Column(DataType.TEXT)
    additional_assignee_list: string;

    @Column(DataType.TEXT)
    u_activity_count: string;

    @Column(DataType.TEXT)
    u_resp: string;

    @Column(DataType.TEXT)
    notify: string;

    @Column(DataType.TEXT)
    service_offering: string;

    @Column(DataType.TEXT)
    sys_class_name: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    closed_by: string;

    @Column(DataType.TEXT)
    follow_up: string;

    @Column(DataType.TEXT)
    reopened_by: string;

    @Column(DataType.TEXT)
    reassignment_count: string;

    @Column(DataType.TEXT)
    u_inter_empresa: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    assigned_to: string;

    @ForeignKey(() => ProductType)
    @Column({
        type: DataType.TEXT
    })
    u_product_type: string;
    
    @Column(DataType.TEXT)
    sla_due: string;
    
    @ForeignKey(() => CoreCompany)
    @Column({
        type: DataType.TEXT
    })
    u_companyincidencia: string
            
    @Column(DataType.TEXT)
    u_group_history: string;

    @ForeignKey(() => Category)
    @Column({
        type: DataType.TEXT
    })
    u_category: string
            
    @Column(DataType.TEXT)
    u_reopen_count: string;

    @Column(DataType.TEXT)
    escalation: string;

    @Column(DataType.TEXT)
    u_reopen_puc: string;

    @Column(DataType.TEXT)
    upon_approval: string;

    @Column(DataType.TEXT)
    u_ibm_event_id: string;

    @Column(DataType.TEXT)
    correlation_id: string;

    @Column(DataType.TEXT)
    timeline: string;

    @Column(DataType.TEXT)
    u_related_incident: string;

    @Column(DataType.TEXT)
    u_edp_problem: string;

    @Column(DataType.TEXT)
    made_sla: string;

    @Column(DataType.TEXT)
    u_communication_sent: string;

    @Column(DataType.TEXT)
    promoted_on: string;

    @ForeignKey(() => Units)
    @Column({
        type: DataType.TEXT
    })
    u_unitincidencia: string

    @Column(DataType.TEXT)
    u_user_tipification: string;

    @Column(DataType.TEXT)
    u_security_incident_type: string;

    @Column(DataType.TEXT)
    u_real_hour: string;

    @Column(DataType.TEXT)
    sys_updated_by: string;

    @Column(DataType.TEXT)
    u_phone: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    opened_by: string;

    @Column(DataType.TEXT)
    user_input: string;

    @Column(DataType.TEXT)
    sys_created_on: string;

    @Column(DataType.TEXT)
    u_gdpr_impact: string;

    @ForeignKey(() => SysUserGroup)
    @Column({
        type: DataType.TEXT
    })
    sys_domain: string;

    @Column(DataType.TEXT)
    proposed_on: string;

    @Column(DataType.TEXT)
    actions_taken: string;

    @Column(DataType.TEXT)
    u_sociedad2incidencia: string;

    @Column(DataType.TEXT)
    calendar_stc: string;

    @Column(DataType.TEXT)
    closed_at: string;

    @Column(DataType.TEXT)
    u_bloqueada: string;

    @Column(DataType.TEXT)
    business_service: string;

    @Column(DataType.TEXT)
    business_impact: string;

    @Column(DataType.TEXT)
    rfc: string;

    @Column(DataType.TEXT)
    time_worked: string;

    @Column(DataType.TEXT)
    expected_start: string;

    @Column(DataType.TEXT)
    u_external_number: string;

    @Column(DataType.TEXT)
    opened_at: string;

    @Column(DataType.TEXT)
    u_breached: string;

    @Column(DataType.TEXT)
    work_end: string;

    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    caller_id: string;
    
    @Column(DataType.TEXT)
    reopened_time: string;

    @Column(DataType.TEXT)
    subcategory: string;

    @Column(DataType.TEXT)
    work_notes: string;

    @Column(DataType.TEXT)
    u_duplicado: string;
    
    @ForeignKey(() => SysUser)
    @Column({
        type: DataType.TEXT
    })
    u_gestor_de_incidencias: string;

    @ForeignKey(() => SysUserGroup)
    @Column({
        type: DataType.TEXT
    })
    assignment_group: string;
    
    @Column(DataType.TEXT)
    u_defect: string;

    @Column(DataType.TEXT)
    business_stc: string;

    @Column(DataType.TEXT)
    cause: string;

    @Column(DataType.TEXT)
    description: string;

    @Column(DataType.TEXT)
    calendar_duration: string;

    @Column(DataType.TEXT)
    close_notes: string;

    @Column(DataType.TEXT)
    u_vip: string;

    @Column(DataType.TEXT)
    sys_id: string;

    @Column(DataType.TEXT)
    u_assignment_globalsd: string;

    @Column(DataType.TEXT)
    contact_type: string;

    @Column(DataType.TEXT)
    incident_state: string;

    @Column(DataType.TEXT)
    urgency: string;

    @Column(DataType.TEXT)
    problem_id: string;

    @Column(DataType.TEXT)
    u_category_count: string;

    @Column(DataType.TEXT)
    u_priority_counter: string;

    @ForeignKey(() => CoreCompany)
    @Column({
        type: DataType.TEXT
    })
    company: string;

    @Column(DataType.TEXT)
    activity_due: string;

    @Column(DataType.TEXT)
    severity: string;

    @Column(DataType.TEXT)
    overview: string;

    @Column(DataType.TEXT)
    comments: string;

    @Column(DataType.TEXT)
    approval: string;

    @Column(DataType.TEXT)
    due_date: string;

    @Column(DataType.TEXT)
    sys_mod_count: string;

    @Column(DataType.TEXT)
    u_group_check: string;

    @Column(DataType.TEXT)
    u_sociedadincidencia: string;

    @Column(DataType.TEXT)
    sys_tags: string;

    @Column(DataType.TEXT)
    u_resolution_code: string;

    @ForeignKey(() => CMNLocation)
    @Column({
        type: DataType.TEXT
    })
    u_locationincidencia: string;

    @Column(DataType.TEXT)
    u_edp_critical_ci: string;

    @Column(DataType.TEXT)
    u_globalsd_count: string;

    @Column(DataType.TEXT)
    u_masiva: string;

    @ForeignKey(() => CMNLocation)
    @Column({
        type: DataType.TEXT
    })
    location: string;

    @Column(DataType.TEXT)
    u_cont_adjuntos: string;

    @Column(DataType.TEXT)
    category: string;
    
    @BelongsTo(() => SysUser)
    u_operadorBto: SysUser;

    @BelongsTo(() => Subcategory)
    subcategoryBto: Subcategory;

    @BelongsTo(() => SysUser)
    callerBto: SysUser;

    @BelongsTo(() => SysUser)
    assignedtoBto: SysUser;

    @BelongsTo(() => ProductType)
    product_typeBto: ProductType;

    @BelongsTo(() => CoreCompany)
    companyincidenciaBto: CoreCompany;

    @BelongsTo(() => Category)
    categoryBto: Category;

    @BelongsTo(() => Units)
    unitincidencia: Units;

    @BelongsTo(() => SysUser)
    opened_byBto: SysUser;

    @BelongsTo(() => SysUserGroup)
    sys_domainBto: SysUserGroup;

    @BelongsTo(() => SysUser)
    caller_idBto: SysUser;

    @BelongsTo(() => SysUser)
    gestor_de_incidenciasBto: SysUser;

    @BelongsTo(() => SysUserGroup)
    assignment_groupBto: SysUserGroup;

    @BelongsTo(() => CoreCompany)
    companyBto: CoreCompany;

    @BelongsTo(() => CMNLocation)
    u_locationincidenciaBto: CMNLocation;

    @BelongsTo(() => CMNLocation)
    locationBto: CMNLocation;

    @BelongsTo(() => SysUser)
    closed_byBto: SysUser;
    
}