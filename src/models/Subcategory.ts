import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "subcategory"
})

/**
 * Class that represents the subcategory model
 */
export class Subcategory extends Model<Subcategory> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'u_subcategory')
    incident: Incident[];
}