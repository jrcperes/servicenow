import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "cmn_location"
})

/**
 * Class that represents the CMNLocation type model
 */
export class CMNLocation extends Model<CMNLocation> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'u_locationincidencia')
    u_locationincidencia_incident: Incident[];

    @HasMany(() => Incident, 'location')
    location_incident: Incident[];
    
}