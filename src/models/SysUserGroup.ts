import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "sys_user_group"
})

/**
 * Class that represents the SysUserGroup type model
 */
export class SysUserGroup extends Model<SysUserGroup> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'sys_domain')
    sys_domain_incident: Incident[];

    @HasMany(() => Incident, 'assignment_group')
    assignment_group_incident: Incident[];
    
}