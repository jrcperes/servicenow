import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "product_type"
})

/**
 * Class that represents the product type model
 */
export class ProductType extends Model<ProductType> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'u_product_type')
    u_product_type_incident: Incident[];
}