import { Table, Model, PrimaryKey, HasMany, DataType, Column, Scopes } from 'sequelize-typescript';

import { Incident } from "./Incident";

@Scopes({
    full:{
        attributes: ['value', 'link']
    }
})
@Table({
    timestamps: false,
    tableName: "core_company"
})

/**
 * Class that represents the core company type model
 */
export class CoreCompany extends Model<CoreCompany> {

    @PrimaryKey
    @Column(DataType.TEXT)
    value: string;

    @Column(DataType.TEXT)
    link: string;

    @HasMany(() => Incident, 'u_companyincidencia')
    u_companyincidencia_incident: Incident[];

    @HasMany(() => Incident, 'company')
    company_incident: Incident[];
    
}