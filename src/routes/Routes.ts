import { Application } from 'express';
import { ImportController } from '../controllers/ImportController';

/**
 * Class that defines API routes
 */
class Routes {
    
    private importController: ImportController;
    
    constructor() {        
        this.importController = new ImportController();
    }
    
    /**
     * 
     * @param app: Defining API routes
     */
    public initRoutes(app: Application): void {

        /**
         * Route to import
        */
        app.route('/import/:table').get(this.importController.import);
    }

}

export default new Routes();