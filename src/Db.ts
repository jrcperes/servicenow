import {Sequelize} from 'sequelize-typescript';
const config = require('./config/env/config')();

/**
 * Class that initiates the database configuration (Sequelize)
 */

class Db {

    public sequelize: Sequelize;

    constructor() {
        const params = {
            database: config.db,
            dialect: config.dialect,
            host: config.host,
            port: config.dbPort,
            username: config.username,
            password: config.password,
            modelPaths: [`${__dirname}/models`],
            dialectOptions: {
                options: {
                    trustServerCertificate: true,
                    encrypt: true,
                }
            }
        }

        this.sequelize = new Sequelize(params);
    }

}

export default new Db().sequelize;